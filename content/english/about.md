---
title : "KNOW MORE ABOUT<br> THE WEBONES"
image : "images/backgrounds/portrait.jpg"
# button
button:
  enable : true
  label : "DOWNLOAD CV"
  link : "#"

########################### Experience ##############################
experience:
  enable : true
  title : "EXPERIENCE"
  experience_list:
  # experience item loop
    - name : "FREELANCER"
      duration : "2020-Act"
      content : "Creation of Themes for Wordpress"
      web : "https://new.sporcks.com/ <br>
       https://wiliercr.com/<br>
       https://riversspencer.com/"

  # experience item loop
    - name : "AUGE CRM"
      company : "https://paginasauge.com"
      duration : "2020-2021"
      content : "Working with HTML, CSS and Javascript in the HUGO static site builder"
      web : "https://supertiressilao.com.mx <br>
       https://centraldellantas.com<br>
       https://www.seicca.com.mx"

       # experience item loop
    - name : "Online Cuba"
      company : "https://onlinecubahosting.com"
      duration : "2018-2019"
      content : "Web developer and SEO analyst with Wordpress"
      web : "https://estudiosraro.com <br>
       https://pedro.onlinecubahosting.com<br>
       https://www.divulgata.com"
      
    

############################### Skill #################################
skill:
  enable : true
  title : "SKILL"
  skill_list:
    # skill item loop
    - name : "HTML"
      percentage : "98%"
      
    # skill item loop
    - name : "CSS"
      percentage : "98%"
      
    # skill item loop
    - name : "Java Script"
      percentage : "95%"
      
    # skill item loop
    - name : "Wordpress"
      percentage : "98%"

       # skill item loop
    - name : "HUGO"
      percentage : "98%"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---


We are The WebOnes, professional web developers. Writing code is our passion. We are here to help you design your website. No matter how difficult your site is, I will always find an easy way to meet your needs. Stay connected with us........😊