---
title : "SABER MÁS ACERCA <br>THE WEBONES"
image : "images/backgrounds/portrait.jpg"
# button
button:
  enable : true
  label : "DESCARGAR CV"
  link : "#"

########################### Experience ##############################
experience:
  enable : true
  title : "EXPERIENCIA"
  experience_list:
    # experience item loop
    - name : "FREELANCER"
      duration : "2020-Act"
      content : "Creacion de Themes para Wordpress"
      web : "https://new.sporcks.com/ <br>
       https://wiliercr.com/<br>
       https://riversspencer.com/"

    # experience item loop
    - name : "AUGE CRM"
      company : "https://paginasauge.com<br>"
      duration : "2020-2021"
      content : "Trabajando con HTML, CSS y Javascript en el generador de sitios estáticos HUGO"
      web : "https://safewheel.com.mx <br>
       https://www.llantyparsanfco.com<br>
       https://www.seicca.com.mx"

    # experience item loop
    - name : "Online Cuba"
      company : "https://onlinecubahosting.com<br>"
      duration : "2018-2019"
      content : "Desarrolador web y analista Seo con Wordpress"
      web : "https://estudioraro.com <br>
       https://pedro.onlinecubahosting.com<br>
       https://www.divulgata.com"
      
      
  

############################### Skill #################################
skill:
  enable : true
  title : "HABILIDADES"
  skill_list:
    # skill item loop
    - name : "HTML"
      percentage : "98%"
      
    # skill item loop
    - name : "CSS"
      percentage : "98%"
      
    # skill item loop
    - name : "Java Script"
      percentage : "95%"

    # skill item loop
    - name : "PHP"
      percentage : "95%"
      
    # skill item loop
    - name : "Wordpress"
      percentage : "98%"

       # skill item loop
    - name : "HUGO"
      percentage : "98%"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---

Somos The WebOnes, desarrolladores web profesional. Escribir código es nuestra pasión. Estamos aquí para ayudarte a diseñar tu página web. No importa cuán difícil sea su sitio, siempre encontraré una manera fácil para satifacer tus necesidades. Mantente conectado con nosotros......😊